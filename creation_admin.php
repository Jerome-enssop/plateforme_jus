<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Modif recette</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";

$recupIdRecette = isset($_GET["id"])?$_GET["id"] : "";
$recupIdIngredient = isset($_GET["id_ingredient"])?$_GET["id_ingredient"] : "";


try {
    $req = $bdd->prepare("SELECT *, recette.nom AS nom_recette, ingredient.nom AS nom_ingredient FROM `ingredient_recette` INNER JOIN ingredient ON ingredient.id_ingredient = ingredient_recette.id_ingredient 
    INNER JOIN recette ON recette.id_recette = ingredient_recette.id_recette WHERE recette.id_recette=?"); 
    $req->execute([$recupIdRecette]);
    $results = $req->fetchAll(); 
    $stockIngredient = $results[0];
}

catch(PDOException $e) {
    echo "Erreur select: ".$e->getMessage();
}

?>

<h2 id='h2AjoutIngredientAdmin'><?php echo $stockIngredient['nom_recette'];?></h2>


<section>


<h2>Les ingrédients</h2>

<?php

$supIngredient = isset($_GET["sup"])?$_GET["sup"] : "";


foreach ($results as $portion) {
    echo "<p>- ".$portion['nb_portion']." portions de ".$portion['nom_ingredient']."<a href='?sup=ok&id=".$portion['id_recette']."&id_ingredient=".$portion['id_ingredient']."'>x</a></p><br>";
}

if ($supIngredient == 'ok') {
    try {
        $req = $bdd->prepare("DELETE FROM ingredient_recette WHERE id_ingredient = ? && id_recette= ?" );
        $req->execute([$recupIdIngredient, $recupIdRecette]); 
        header("Location: creation_admin.php?id=".$stockIngredient['id_recette']);
    }
    catch(PDOException $e) {
        echo "Erreur suppression: ".$e->getMessage();
    }
}

$recupQuantite = isset($_POST['quantite']) && !empty($_POST['quantite']) ? $_POST['quantite']: "";
$recupIngredient = isset($_POST['listeIngredient']) && !empty($_POST['listeIngredient']) ? $_POST['listeIngredient']: "";


?>

<form action="" method="post">
    <p>Quantité : </p><br> <input type="number" name="quantite"><p> portions de</p>
    <select name="listeIngredient">
        <?php

        try {
            $req = $bdd->prepare("SELECT nom, id_ingredient FROM ingredient"); 
            $req->execute();
            $results = $req->fetchAll();
            foreach($results as $ingredient) {
                echo "<option value='".$ingredient['id_ingredient']."'>".$ingredient['nom']."</option>";
            }
        }
        
        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }

        ?>
    </select>
    <input  type="submit" name="submit" id="sauvegarder" value="Ajouter">
</form>

<?php

if (isset($_POST['submit'])){
    if (isset($_POST['quantite']) && !empty($_POST['quantite']) && isset($_POST['listeIngredient']) && !empty($_POST['listeIngredient'])) {
        try {
            $req = $bdd->prepare("INSERT INTO ingredient_recette (id_recette, id_ingredient, nb_portion) VALUES(?,?,?)"); 
            $req->execute([$recupIdRecette, $recupIngredient, $recupQuantite]);
            header("Location: ");
            }

        catch(PDOException $e) {
            echo "Vous ne pouvez pas ajouter de nouveau cet ingrédient. Sinon veuillez le supprimer préalablement";
        }
    } else {
        echo '<p>Veuillez remplir tous les champs SVP</p>';
    }
}

?>

<a href="liste_admin.php">Valider la recette</a>

</section>
    
</body>
</html>