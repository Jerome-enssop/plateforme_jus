<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Modif recette</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";



$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupPhoto = isset($_FILES["photo"]) ? "jus/fruit_emoticons/pngs/".microtime().$_FILES["photo"]["name"]: "";
$recupIdIngredient = isset($_GET["id"])?$_GET["id"] : "";

try {
    $req = $bdd->prepare("SELECT * FROM recette WHERE id_recette=$recupIdIngredient"); 
    $req->execute();
    $results = $req->fetchAll(); 
    $stockIngredient = $results[0];
}

catch(PDOException $e) {
    echo "Erreur select: ".$e->getMessage();
}

?>

<h2 id='h2AjoutIngredientAdmin'>Modifier : <?php echo $stockIngredient['nom'];?></h2>

<form  enctype="multipart/form-data" action="" method="post">
    <div id="ingredientAdmin">
        <div class="divForm"><p>Nom</p> <input type="text" name="nom" value="<?php echo $stockIngredient['nom'];?>"><br>
            <p>Description</p><textarea name="description" placeholder="" rows="5" cols="33"><?php echo $stockIngredient['descriptions'];?></textarea> <br>
        </div>
        <div class="divForm"><p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
        </div>
    </div>
    <input id="submitAjoutIngredientAdmin" type="submit" name="submit" id="sauvegarder" value="Editer les ingrédients">
</form>

<?php

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['description']) && !empty($_POST['description']) && isset($_FILES['photo']) && !empty($_FILES['photo'])) {
        try {
            $req = $bdd->prepare("UPDATE recette SET nom=?, descriptions=?, photo=? WHERE id_recette=$recupIdIngredient"); 
            $req->execute([$recupNom, $recupDescription, $recupPhoto]);
            header("Location: creation_admin.php?id=".$stockIngredient['id_recette']);
            }

        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
    } else {
        echo '<p>Veuillez remplir tous les champs SVP</p>';
    }
}



?>
    
</body>
</html>