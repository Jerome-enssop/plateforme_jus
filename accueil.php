<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style_visiteur.css">
    <title>Accueil</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav.php";

?>

<p id="pTitreAccueil">Vous êtes plutôt: <a href="triage_recette_fruit.php">Jus de fruit /</a><a href="triage_recette_legume.php"> Jus de legume</a></p>
   
<div id="cadreAccueil">

<?php

try {
    $req = $bdd->prepare("SELECT nom, photo FROM recette"); 
    $req->execute();
    $results = $req->fetchAll();
    foreach($results as $recette) {
        echo "<a href='presentation_recette.php'><img src='".$recette['photo']."'><p>".$recette['nom']."</p></a>";
    }
}

catch(PDOException $e) {
    echo "Erreur insert into: ".$e->getMessage();
}

?>

</div>

<p id="pPiedAccueil">Vous êtes prêts à partager vos idées créatives?</p>
<div id="aPiedAccueil"><a  href="creation.php">Soumettez vos recettes de jus</a><div>
</body>
</html>