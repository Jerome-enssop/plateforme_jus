<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupPhoto = isset($_FILES["photo"]) ? "jus/fruit_emoticons/pngs/".microtime().$_FILES["photo"]["name"]: "";
$recupType = isset($_POST['type']) && !empty($_POST['type']) ? $_POST['type']: "";

?>

<h2 id='h2AjoutIngredientAdmin'>Ajouter un ingrédient</h2>

<form  enctype="multipart/form-data" action="" method="post">
    <div id="ingredientAdmin">
        <div class="divForm"><p>Nom</p> <input type="text" name="nom" placeholder=""><br>
            <p>Description</p><textarea name="description" placeholder="" rows="5" cols="33"></textarea> <br>
        </div>
        <div class="divForm"><p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
        <p>Type</p><select name="type">
                        <option value='1'>Fruit</option>
                        <option value='2'>Légume</option>
                        <option value='3'>Epice</option>
                    </select></div>
    </div>
    <input id="submitAjoutIngredientAdmin" type="submit" name="submit" id="sauvegarder" value="Sauvegarder">
</form>

<?php

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['description']) && !empty($_POST['description']) && isset($_FILES['photo']) && !empty($_FILES['photo']) && isset($_POST['type']) && !empty($_POST['type'])) {
        try {
            $req = $bdd->prepare("INSERT INTO ingredient (nom, descriptions, photo, id_type) VALUES (?,?,?,?)"); 
            $req->execute([$recupNom, $recupDescription, $recupPhoto, $recupType]);
            move_uploaded_file($_FILES["photo"]["tmp_name"], $recupPhoto);
            header("Location: liste_admin.php");
            }

        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
    } else {
        echo '<p>Veuillez remplir tous les champs SVP</p>';
    }
}

?>

    
</body>
</html>