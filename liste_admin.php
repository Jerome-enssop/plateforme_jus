<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Liste_admin</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";

?>

<div id="divListe">
    <div>
        <h4>Ingrédients <a href="ajout_ingredient_admin.php">Ajouter</a></h4>
        <?php
        try {
            $req = $bdd->prepare("SELECT nom,id_ingredient FROM ingredient"); 
            $req->execute();
            $results = $req->fetchAll();
            foreach ($results as $ingredient) {
                echo "<p><a href='modif_ingredient_admin.php?id=".$ingredient['id_ingredient']."'>".$ingredient['nom']."</a></p><hr>";
            }
        }
        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
        ?>
    </div>

    <div>
        <h4>Recettes</h4>
        <?php
        try {
            $req = $bdd->prepare("SELECT nom, id_recette FROM recette"); 
            $req->execute();
            $results = $req->fetchAll();
            foreach ($results as $recette) {
                echo "<p><a href='modif_recette_admin.php?id=".$recette['id_recette']."'>".$recette['nom']."</a></p><hr>";
            }
        }
        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
        ?>
    </div>

</div>
    
</body>
</html>