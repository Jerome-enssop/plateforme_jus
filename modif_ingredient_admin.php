<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Modif Ingrédient</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";



$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom']: "";
$recupDescription = isset($_POST['description']) && !empty($_POST['description']) ? $_POST['description']: "";
$recupPhoto = isset($_FILES["photo"]) ? "jus/fruit_emoticons/pngs/".microtime().$_FILES["photo"]["name"]: "";
$recupType = isset($_POST['type']) && !empty($_POST['type']) ? $_POST['type']: "";
$recupIdIngredient = isset($_GET["id"])?$_GET["id"] : "";

try {
    $req = $bdd->prepare("SELECT * FROM ingredient WHERE id_ingredient=$recupIdIngredient"); 
    $req->execute();
    $results = $req->fetchAll(); 
    $stockIngredient = $results[0];
}

catch(PDOException $e) {
    echo "Erreur select: ".$e->getMessage();
}

?>

<h2 id='h2AjoutIngredientAdmin'>Modifier : <?php echo $stockIngredient['nom'];?></h2>

<form  enctype="multipart/form-data" action="" method="post">
    <div id="ingredientAdmin">
        <div class="divForm"><p>Nom</p> <input type="text" name="nom" value="<?php echo $stockIngredient['nom'];?>"><br>
            <p>Description</p><textarea name="description" placeholder="" rows="5" cols="33"><?php echo $stockIngredient['descriptions'];?></textarea> <br>
        </div>
        <div class="divForm"><p>Photo de l'ingredient</p><input type="file" name="photo" placeholder=""> <br>
        <p>Type</p><select name="type">
                        <option value='1' <?php if ($stockIngredient['id_type'] == 1){echo "selected";}else{echo "";}?>>Fruit</option>
                        <option value='2' <?php if ($stockIngredient['id_type'] == 2){echo "selected";}else{echo "";}?>>Légume</option>
                        <option value='3' <?php if ($stockIngredient['id_type'] == 3){echo "selected";}else{echo "";}?>>Epice</option>
                    </select></div>
    </div>
    <input id="submitAjoutIngredientAdmin" type="submit" name="submit" id="sauvegarder" value="Sauvegarder">
</form>

<?php

if (isset($_POST['submit'])){
    if (isset($_POST['nom']) && !empty($_POST['nom']) && isset($_POST['description']) && !empty($_POST['description']) && isset($_FILES['photo']) && !empty($_FILES['photo']) && isset($_POST['type']) && !empty($_POST['type'])) {
        try {
            $req = $bdd->prepare("UPDATE ingredient SET id_type=?, nom=?, descriptions=?, photo=? WHERE id_ingredient=$recupIdIngredient"); 
            $req->execute([$recupType, $recupNom, $recupDescription, $recupPhoto]);
            header("Location: liste_admin.php");
            }

        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
    } else {
        echo '<p>Veuillez remplir tous les champs SVP</p>';
    }
}

?>
    
</body>
</html>