<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Connexion_admin</title>
</head>
<body>

<?php

$servName = "localhost";
$dbname = "recette_jus";
$user = "admin";
$pass = "mdp";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

}
catch(PDOException $e) {
    echo "Erreur de connexion: ".$e->getMessage();
}

include "nav_admin.html";



?>

<div id="formulaire">
    <p>Connexion</p>
    <form id="connexion" method="get" action="">
        <p>Identifiant</p><input type="text" name="identifiant" placeholder=""><br>
        <p>Mot de passe</p><input type="password" name="mdp" placeholder=""><br>
        <input class="submit" type="submit" name="submit" id="Envoyer" value="Connexion">
    </form>
</div>

<?php

$recupIdentifiant = isset($_GET['identifiant']) && !empty($_GET['identifiant']) ? $_GET['identifiant']: "";
$recupMdp = isset($_GET['mdp']) && !empty($_GET['mdp']) ? $_GET['mdp']: "";

if (isset($_GET['submit'])){
    if (isset($_GET['identifiant']) && !empty($_GET['identifiant']) && isset($_GET['mdp']) && !empty($_GET['mdp'])) {
        try {
            $req = $bdd->prepare("SELECT * FROM administrateur"); 
            $req->execute();
            $results = $req->fetchAll();
            $identifiantValid = null;
            foreach ($results as $admin) {
                if ($recupIdentifiant == $admin["identifiant"] && $recupMdp == $admin["mdp"]) {
                    header("Location: liste_admin.php");
                    $identifiantValid = true;
                } 
            }
            if ($identifiantValid != true) {
                echo "<p class='erreur'>L'identifiant ou le mot de passe n'est pas valide</p>";
            }
            
        }
            

        catch(PDOException $e) {
            echo "Erreur insert into: ".$e->getMessage();
        }
    } else {
        echo "<p class='erreur'>Veuillez remplir tous les champs SVP</p>";
    }
}



?>


    
</body>
</html>